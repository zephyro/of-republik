<!doctype html>
<html class="no-js" lang="">
    <head>
        <base href="{$_modx->config.site_url}">

        <title>{$_modx->resource.pagetitle}</title>

        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="{$_modx->resource.introtext | replace :' "':' «' | replace :'"':'»'}">
        <meta name="viewport" content="width=1250">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="{$_modx->config.assets_url}template/js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="{$_modx->config.assets_url}template/js/vendor/swiper/css/swiper.min.css">
        
        <link rel="stylesheet" href="{$_modx->config.assets_url}template/css/main.css">

    </head>
    <body>
    
        <div class="page">
        
            <header class="header">
                <div class="header__top">
                    <div class="container">
                        <div class="header__logo">
                            <img src="{$_modx->config.assets_url}template/img/logo.png" class="img-fluid" alt="">
                        </div>
                        
                        <div class="header__lng">
                            <a href="/">Ro</a>
                            <a class="active" href="{$_modx->config.site_url}ru/">ru</a>
                        </div>
                        
                        <a class="header__phone" href="tel:{$_modx->resource.main_phone}">{$_modx->resource.main_phone}</a>
                        
                    </div>
                </div>
                <div class="header__slider swiper-container">
                    <div class="swiper-wrapper">

                        {set $rows = json_decode($_modx->resource.header_slider, true)}
                        {foreach $rows as $row}
                            <div class="swiper-slide">
                                <div class="header__slide" style="background-image: url('{$row.image}')">
                                    <div class="container">
                                        <div class="header__title">{$row.header}</div>
                                        <div class="header__text">{$row.text}</div>
                                        <a href="#fourth" class="btn btn_scroll">{$_modx->resource.header_button}</a>
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>
            </header>
            
            <section class="first">
                <div class="container">
                    <div class="first__heading">{$_modx->resource.first_heading}</div>
                    <ul class="first__list">

                        {set $rows = json_decode($_modx->resource.first_data, true)}
                        {foreach $rows as $row}
                            <li style="background-image: url('{$row.icon}');">
                                <h4>{$row.title}</h4>
                                <p>{$row.text}</p>
                            </li>
                        {/foreach}

                    </ul>
                </div>
            </section>
            
            <section class="second">
                <div class="container">
                    <div class="second__heading">
                        <h2>{$_modx->resource.second_title}</h2>
                        <div class="second__subtitle">{$_modx->resource.second_subtitle}</div>
                        <span class="second__nav second__nav_prev"></span>
                        <span class="second__nav second__nav_next"></span>
                    </div>
    
                    <div class="second__slider swiper-container">
                        <div class="swiper-wrapper">

                            {set $rows = json_decode($_modx->resource.second_gallery, true)}
                            {foreach $rows as $row}
                                <div class="swiper-slide">
                                    <div class="second__slide">
                                        <div class="second__slide_image">
                                            <img src="{$row.image}" class="img-fluid" alt="{$row.title}">
                                        </div>
                                        <div class="second__slide_title">{$row.title}</div>
                                        <div class="second__slide_text">
                                            {$row.text}
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                            
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                    
                </div>
            </section>
            
            <section class="third">
                <div class="container">
                    <div class="third__heading">
                        <h2>{$_modx->resource.third_heading}</h2>
                        <div class="third__subtitle">{$_modx->resource.third_subtitle}</div>
                    </div>
                    <ul class="third__service">
                        {set $rows = json_decode($_modx->resource.third_content, true)}
                        {foreach $rows as $row}
                            <li>
                                <div class="third__service_image">
                                    <img src="{$row.image}" class="img-fluid" alt="{$row.title}">
                                </div>
                                <div class="third__service_name"><span>{$row.title}</span></div>
                            </li>
                        {/foreach}
                    </ul>
                </div>
            </section>
            
            <section class="fourth" id="fourth">
                <div class="container">
                    <div class="fourth__heading">
                        <h2>{$_modx->resource.fourth_heading}</h2>
                        <div class="fourth__subtitle">{$_modx->resource.fourth_subtitle}</div>
                    </div>
                    
                    <div class="fourth__row">
                        
                        <div class="fourth__left">
                            <div class="fourth__form">
                                [[!AjaxForm?
                                    &snippet=`FormIt`
                                    &form=`tpl.form`
                                    &hooks=`email`
                                    &emailSubject=`Сообщение с сайта [[++site_name]]`
                                    &emailFrom=`[[++from_email]]`
                                    &emailTo=`[[++contact_email]]`
                                    &emailTpl=`tpl.form.send`
                                    &validate= `name:required,phone:required`
                                    &validationErrorMessage=`Заполнены не все поля!`
                                    &successMessage=`Ваше сообщение принято! Мы свяжемся с вами в ближайшее время!`
                                ]]
                            </div>
                        </div>
                        
                        <div class="fourth__right">
                            <div class="fourth__image">
                                <img src="{$_modx->resource.fourth_image}" class="img-fluid" alt="">
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </section>
            
            <section class="fifth">
                <div id="map"></div>
            </section>
            
            <section class="sixth">
                <div class="container">
                    <h2>{$_modx->resource.sixth_heading}</h2>
                    <div class="sixth__subtitle">{$_modx->resource.sixth_subtitle}</div>
                    <div class="sixth__address">{$_modx->resource.main_address}</div>
                    <div class="sixth__contact">
                        <a class="sixth__contact_phone" href="tel:{$_modx->resource.main_phone}">{$_modx->resource.main_phone}</a>
                        <a class="sixth__contact_elem whatsapp" href="{$_modx->resource.main_whatsapp}"></a>
                        <a class="sixth__contact_elem viber" href="{$_modx->resource.main_viber}""></a>
                    </div>
                    
                    <div class="social">

                        {set $rows = json_decode($_modx->resource.main_social, true)}
                        {foreach $rows as $row}

                            <a target="_blank" href="{$row.link}" title="{$row.title}">
                                <i class="social_base">
                                    <img src="{$row.imageBase}" class="img-fluid" alt="">
                                </i>
                                <i class="social_hover">
                                    <img src="{$row.imageHover}" class="img-fluid" alt="">
                                </i>
                            </a>

                        {/foreach}

                    </div>
                    
                </div>
            </section>
            
            <footer class="footer">
                <div class="container">
                    Created by <a href="http://fivestars-webdesign.com/ru/">Five Stars</a>
                </div>
            </footer>
        
        </div>

        <!-- Thanks Modal -->

        <div class="hide">
            <a href="#thanks" class="btn_thanks btn_modal"></a>
            <div class="modal" id="thanks">
                <div class="thanks_text">
                    {$_modx->resource.main_order}
                </div>
            </div>
        </div>

        <script src="{$_modx->config.assets_url}template/js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="{$_modx->config.assets_url}template/js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="{$_modx->config.assets_url}template/js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="{$_modx->config.assets_url}template/js/vendor/swiper/js/swiper.min.js"></script>
        <script src="{$_modx->config.assets_url}template/js/vendor/jquery.scrollTo.min.js"></script>
        

        <script src="{$_modx->config.assets_url}template/js/plugins.js"></script>
        <script src="{$_modx->config.assets_url}template/js/main.js"></script>

    </body>
</html>

