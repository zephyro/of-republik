
$(".btn_modal").fancybox({
    'padding'    : 0
});

$('.btn_scroll').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, 500, {offset:-82 });
    return false;
});


var slider = new Swiper('.header__slider', {
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
});

var gallery = new Swiper('.second__slider', {
    loop: true,
    slidesPerView: 3,
    slidesPerGroup: 3,
    spaceBetween: 40
});


$('.second__nav_prev').on('click touchstart', function(e) {
    e.preventDefault();
    gallery.slidePrev(1000,'');
});

$('.second__nav_next').on('click touchstart', function(e) {
    e.preventDefault();
    gallery.slideNext(1000,'');
});


// Placeholders

$('.form_group input').focus(function(event) {
    $(this).closest('.form_group').addClass('focus');
});

$('.form_group input').focusout(function(){

    var inputVal = $(this).closest('.form_group').find('input').val();
    if (inputVal == '') {
        $(this).closest('.form_group').removeClass('focus');
    }
});


ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
        center: [47.09758918621081,29.072933728836055],
        zoom: 16,
        controls: ['smallMapDefaultSet']
    });

    myMap.geoObjects
        .add(new ymaps.Placemark([47.09758918621081,29.072933728836055], {
            balloonContent: ''
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }));
}
